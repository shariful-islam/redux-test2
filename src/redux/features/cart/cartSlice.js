import { createSlice } from "@reduxjs/toolkit";

const initialState = {};

const cartSlice = createSlice({
	name: "counter",
	initialState,
	reducers: {},
});

export const {} =
	cartSlice.actions;

export default cartSlice.reducer;
